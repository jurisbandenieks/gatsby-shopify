import React from 'react';
import {
  Layout,
  SEO,
  HomepageCollectionsGrid,
  FeaturedProducts,
} from 'components';
import ProductContext from 'context/ProductContext';

const IndexPage = () => {
  const { collections } = React.useContext(ProductContext);

  const collectionWithImage = collections.filter(
    collection => collection?.image?.localFile?.childImageSharp?.fluid
  );

  const featuredProducts = collections.filter(
    collection => collection.title.toLowerCase() === 'featured hats'
  );

  return (
    <Layout>
      <SEO title="Home" />
      <HomepageCollectionsGrid collections={collectionWithImage} />
      {featuredProducts && <FeaturedProducts />}
    </Layout>
  );
};

export default IndexPage;
