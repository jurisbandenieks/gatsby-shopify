import styled from 'styled-components';

export const ImageThumbnailWrapper = styled.div`
  cursor: pointer;
  border: solid 4px ${props => (props.isActive ? 'blue' : '#eee')};
`;
