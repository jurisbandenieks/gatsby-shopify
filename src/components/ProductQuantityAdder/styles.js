import styled from 'styled-components';

export const ProductQuantityWapper = styled.div`
  margin-top: 20px;

  > strong {
    display: block;
    margin-bottom: 10px;
  }

  > form {
    display: flex;
  }
`;
