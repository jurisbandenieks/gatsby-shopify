import React from 'react';
import { Button } from '../Button';
import { Input } from '../Input';
import { ProductQuantityWapper } from './styles';
import CartContext from 'context/CartContext';

export function ProductQuantityAdder({ variantId, available }) {
  const [quantity, setQuantity] = React.useState(1);
  const { updateLineItem } = React.useContext(CartContext);

  const handleQuanityChange = e => {
    setQuantity(e.currentTarget.value);
  };

  const handleSubmit = e => {
    e.preventDefault();
    updateLineItem({ variantId, quantity: parseInt(quantity, 10) });
  };

  return (
    <ProductQuantityWapper>
      <strong>Quantity</strong>
      <form onSubmit={handleSubmit}>
        <Input
          disable={!available}
          type="number"
          min="1"
          step="1"
          value={quantity}
          onChange={handleQuanityChange}
        />
        <Button type="submit" disable={!available} fullWidth>
          Add to cart
        </Button>
      </form>
    </ProductQuantityWapper>
  );
}
