import React from 'react';
import { ProductTileWrapper, Description, Title, Pirce } from './styles';
import Img from 'gatsby-image';
import { StyledLink } from '../StyledLink';

export function ProductTile({
  title,
  imageFluid,
  description,
  minPrice,
  handle,
}) {
  return (
    <ProductTileWrapper>
      <Img fluid={imageFluid} />
      <Title>{title}</Title>
      <Description>{description}</Description>
      <Pirce>from ${parseFloat(minPrice).toFixed(2)}</Pirce>
      <StyledLink to={`/products/${handle}`}>View product</StyledLink>
    </ProductTileWrapper>
  );
}
